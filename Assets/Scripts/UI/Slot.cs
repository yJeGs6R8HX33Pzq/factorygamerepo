using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

//UI element for creating objects
public class Slot : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public GameObject prefabReference;
    public IconOptions iconOptions;

    const float dragSpeed = 25;
    GameObject dragObject;
    bool isDrag;
    RaycastHit hit;

    //init
    private void Start()
    {
        GameManager.Instance.OnRotationChanged += UpdateSlotIcon;
    }

    //events
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (GameManager.Instance.toggleMode != ToggleMode.dragDrop) { return; }

        //changes build mode in game manager to prevent deletion
        GameManager.Instance.SetModeBuild();

        //stops the camera
        CameraController.Instance.Deactivate();

        //instantiates the dragObject
        dragObject = Instantiate(prefabReference);
        dragObject.transform.rotation = GameManager.Instance.buildRotation;

        Vector3 targetPosition = GetTargetPosition(eventData.position);
        dragObject.transform.position = targetPosition;

        StartCoroutine(HandleDrag());
    }
    Vector3 velocity;
    IEnumerator HandleDrag()
    {
        Debug.Log("Handle Drag.");
        isDrag = true;
        while (isDrag)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.touches[0].position), out hit))
            {
                float x = Mathf.Round(hit.point.x);
                float height = dragObject.transform.localScale.y;
                float y = Mathf.Round(hit.point.y / (height / 2)) * (height / 2) + height / 2;
                float z = Mathf.Round(hit.point.z);
                Vector3 targetPosition = new Vector3(x, y, z);
                dragObject.transform.position = Vector3.SmoothDamp(dragObject.transform.position, targetPosition, ref velocity, .05f);
                yield return new WaitForEndOfFrame();

            }
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        isDrag = false;

        InteractableObject iO = dragObject.GetComponent<InteractableObject>();
        iO.Solidify();
        dragObject = null;
        SoundManager.PlaySound(SoundManager.Sound.Place);

        //starts the camera
        CameraController.Instance.Activate();
    }


    Vector3 GetTargetPosition(Vector3 _inputPosition)
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(_inputPosition), out hit))
        {
            float x = Mathf.Round(hit.point.x);
            float height = dragObject.transform.localScale.y;
            float y = Mathf.Round(hit.point.y / (height / 2)) * (height / 2) + height / 2;
            float z = Mathf.Round(hit.point.z);
            Vector3 targetPosition = new Vector3(x, y, z);
            return targetPosition;
        }
        else
        {
            return default;
        }
    }


    public void UpdateSlotIcon()
    {
        int rotationIndex = GameManager.Instance.rotationIndex;
        Image img = transform.GetChild(0).GetComponent<Image>();
        img.sprite = iconOptions.GetIconByIndex(rotationIndex);
    }

    public void OnDrag(PointerEventData eventData)
    {
    }
}
