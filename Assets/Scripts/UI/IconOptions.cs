using UnityEngine;

[CreateAssetMenu(menuName = "UITools/IconOptions", fileName = "New Icon Options")]
public class IconOptions : ScriptableObject
{
    public Sprite[] icons;
    public Sprite GetIconByIndex(int _index)
    {
        return icons[_index];
    }
}
