using UnityEngine;
using UnityEngine.UI;

public static class SoundManager
{

    public enum Sound
    {
        Click,
        Place
    }
    private static GameObject oneShotGameObject;
    private static AudioSource oneShotAudioSource;
    public static void PlaySound(Sound sound)
    {
        if (oneShotGameObject == null)
        {
            oneShotGameObject = new GameObject("Sound");
            oneShotAudioSource = oneShotGameObject.AddComponent<AudioSource>();
        }

        if (oneShotAudioSource.pitch != 1) { oneShotAudioSource.pitch = 1; }
        oneShotAudioSource.PlayOneShot(GetAudioClip(sound));
    }
    public static void PlaySound(Sound sound, float pitch)
    {
        Debug.Log("play overloaded method.");

        if (oneShotGameObject == null)
        {
            oneShotGameObject = new GameObject("Sound");
            oneShotAudioSource = oneShotGameObject.AddComponent<AudioSource>();
        }

        oneShotAudioSource.clip = GetAudioClip(sound);
        oneShotAudioSource.pitch = pitch;
        oneShotAudioSource.Play();
    }
    public static AudioClip GetAudioClip(Sound sound)
    {
        foreach (GameAssets.SoundAudioClip soundAudioClip in GameAssets.instance.soundAudioClips)
        {
            if (soundAudioClip.sound == sound)
            {
                return soundAudioClip.clip;
            }
        }

        Debug.Log("error in sound manager : no valid clip matching input sound.");
        return null;
    }
    public static void AddButtonSounds(this Button button)
    {
        button.onClick.AddListener(delegate () { PlaySound(Sound.Click); });
    }
}
