using UnityEngine;

public class GameAssets : MonoBehaviour
{
    private static GameAssets _instance;
    public static GameAssets instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (Resources.Load("GameAssets") as GameObject).GetComponent<GameAssets>();
            }
            return _instance;
        }
    }

    [Header("BLOCK PREFABS")]

    [Header("AUDIO CLIPS")]
    public SoundAudioClip[] soundAudioClips;

    [System.Serializable]
    public class SoundAudioClip
    {
        public SoundManager.Sound sound;
        public AudioClip clip;
    }
}
