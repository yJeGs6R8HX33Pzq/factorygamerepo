using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class CameraController : Singleton<CameraController>
{
    [SerializeField] float moveSpeed;
    [SerializeField] float dragDstThreshold;


    Vector2 referencePoint;
    Vector3 velocity;

    //active state handling
    bool isActive = true;
    public void Activate()
    {        
        StartCoroutine(DelayedActivate());
    }
    IEnumerator DelayedActivate()
    {
        yield return new WaitForSeconds(.1f);
        isActive = true;
    }
    public void Deactivate()
    {
        isActive = false;
    }

    RaycastHit hit;
    [SerializeField] GameObject prefabReference;
    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject != null) { return; }

        if (GameManager.Instance.toggleMode != ToggleMode.tapToPlace) { return; }

        if (GameManager.Instance.editMode != EditMode.build) { return; }

        if (Input.touchCount == 1)
        {
            Touch t = Input.touches[0];
            if (t.phase == TouchPhase.Began)
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(t.position), out hit)) 
                {
                    GameObject dragObject = Instantiate(prefabReference);
                    dragObject.transform.rotation = GameManager.Instance.buildRotation;

                    float x = Mathf.Round(hit.point.x);
                    float height = dragObject.transform.localScale.y;
                    float y = Mathf.Round(hit.point.y / (height / 2)) * (height / 2) + height / 2;
                    float z = Mathf.Round(hit.point.z);
                    Vector3 targetPosition = new Vector3(x, y, z);
                    dragObject.transform.position = targetPosition;
                    dragObject.GetComponent<InteractableObject>().Solidify();
                    SoundManager.PlaySound(SoundManager.Sound.Place);
                }
            }
        }
    }
    void LateUpdate()
    {
        if (EventSystem.current.currentSelectedGameObject != null) { return; }

        if (!isActive) { return; }

        //main controls
        if (Input.touchCount == 1)
        {
            Touch t = Input.touches[0];
            if (t.phase == TouchPhase.Began)
            {
                referencePoint = t.position;
            }
            else
            {

                float dst = Vector2.Distance(referencePoint, t.position);
                if (dst < dragDstThreshold)
                {
                    return;
                }

                //handle panning
                Vector2 deltaTouch = referencePoint - t.position;
                float ang = Vector2.SignedAngle(Vector2.up, deltaTouch) * -1;
                Vector3 edFwd = new Vector3(transform.forward.x, 0, transform.forward.z);
                Vector3 planarForce = Quaternion.AngleAxis(ang + 45, transform.up) * edFwd * -1;
                Vector3 targetPosition = transform.position + planarForce * (deltaTouch.magnitude / 1000f * moveSpeed);
                //transform.position += planarForce * (deltaTouch.magnitude / 1000f * moveSpeed);
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, .01f);
            }
        }


        //rotation logic
        //Vector2 rotateVec = (Input.touches[0].position - referencePoint);
        //if (Mathf.Abs(rotateVec.x) > 12)
        //{
        //    Debug.Log("rotate!");
        //}
        //Rotate(rotateVec.x / 2);

        //zoom logic
        //Vector2 zoomVec = (Input.touches[0].position - referencePoint);
        //if (Mathf.Abs(zoomVec.y) > 12)
        //{
        //    Debug.Log("zoom!");
        //}
        //Zoom(zoomVec.y / -2);
    }
}
