using UnityEngine;
using UnityEngine.EventSystems;


public enum EditMode { build, rotate, delete}
public enum ToggleMode { dragDrop, tapToPlace}

public class GameManager : Singleton<GameManager>
{
    public EditMode editMode = EditMode.build;
    public ToggleMode toggleMode = ToggleMode.dragDrop;

    public int rotationIndex;
    public Quaternion buildRotation;


    //init
    private void Start()
    {
        rotationIndex = 0;
        SetBuildRotation();
        OnRotationChanged += SetBuildRotation;
    }

    //rotation
    public void RotateLeft()
    {
        //decrement the rotation index
        rotationIndex -= 1;

        //checks against the lower bounds of the array length
        if (rotationIndex < 0)
        {
            rotationIndex = 3;
        }

        //change the build rotation using the build index
        buildRotation *= Quaternion.Euler(0, rotationIndex * 90, 0);

        //invoke the rotation changed delegate
        OnRotationChanged?.Invoke();
    }
    public void RotateRight()
    {
        //increment the rotation index
        rotationIndex += 1;

        //checks against the upper bounds of the array length
        if (rotationIndex > 3)
        {
            rotationIndex = 0;
        }

        //change the build rotation using the build index
        buildRotation *= Quaternion.Euler(0, rotationIndex * 90, 0);

        //invoke the rotation changed delegate
        OnRotationChanged?.Invoke();
    }
    public void SetBuildRotation()
    {
        buildRotation = Quaternion.Euler(0, rotationIndex * 90, 0);
    }

    //build modes
    public void SetModeBuild()
    {
        editMode = EditMode.build;
    }
    public void SetModeDelete()
    {
        editMode = EditMode.delete;
    }

    //subscribed to in Slot
    public delegate void RotationChangedDelegate();
    public RotationChangedDelegate OnRotationChanged;


    public void SetToggleDragDrop()
    {
        toggleMode = ToggleMode.dragDrop;
    }
    public void SetToggleTapToPlace()
    {
        toggleMode = ToggleMode.tapToPlace;
    }



    private void Update()
    {
        //if (EventSystem.current.currentSelectedGameObject != null)
        //{
        //    return;
        //}

        ////pan
        //if (Input.touchCount == 1)
        //{
        //    Touch t = Input.touches[0];
        //    if (t.phase == TouchPhase.Began)
        //    {
        //        referencePoint = t.position;
        //    }
        //    else
        //    {
        //        float dst = Vector2.Distance(referencePoint, t.position);
        //        if (dst < 10f)
        //        {
        //            return;
        //        }

        //        //pan logic
        //        Vector2 deltaTouch = referencePoint - t.position;
        //        float ang = Vector2.SignedAngle(Vector2.up, deltaTouch) * -1;
        //        Vector3 edFwd = new Vector3(editorCamera.transform.forward.x, 0, editorCamera.transform.forward.z);
        //        Vector3 planarForce = Quaternion.AngleAxis(ang, transform.up) * edFwd * -1;
        //        editorCamera.transform.position += planarForce * (deltaTouch.magnitude / 500f);


        //        //rotation logic
        //        //Vector2 rotateVec = (Input.touches[0].position - referencePoint);
        //        //if (Mathf.Abs(rotateVec.x) > 12)
        //        //{
        //        //    Debug.Log("rotate!");
        //        //}
        //        //Rotate(rotateVec.x / 2);


        //        //zoom logic
        //        //Vector2 zoomVec = (Input.touches[0].position - referencePoint);
        //        //if (Mathf.Abs(zoomVec.y) > 12)
        //        //{
        //        //    Debug.Log("zoom!");
        //        //}
        //        //Zoom(zoomVec.y / -2);

        //    }
        //}
   

        //    #region build

        //    case EditorMode.build:

        //        if (Input.touchCount == 1)
        //        {
        //            Touch myTouch = Input.touches[0];

        //            if (myTouch.phase == TouchPhase.Began)
        //            {
        //                Ray ray = editorCamera.ScreenPointToRay(myTouch.position);
        //                if (Physics.Raycast(ray, out hit))
        //                {

        //                    //handle the x
        //                    float xNormal = hit.point.x + hit.normal.x;
        //                    float x = Mathf.Round(xNormal);


        //                    //handle the y
        //                    float yRounding = hit.collider.transform.localScale.y;
        //                    float roundedY = ((int)(hit.point.y / yRounding)) * yRounding;
        //                    float y = roundedY + (targetPrefab.transform.localScale.y / 2);


        //                    //handle the z
        //                    float zNormal = hit.point.z + hit.normal.z;
        //                    float z = Mathf.Round(zNormal);

        //                    Vector3 targetPosition = new Vector3(x, y, z);

        //                    //handles the eo
        //                    EditorObject eo = Instantiate(targetPrefab, targetPosition, Quaternion.identity).GetComponent<EditorObject>();
        //                    eo.data.position = targetPosition;
        //                    eo.data.rotation = Quaternion.identity;

        //                    //handles the build FX
        //                    Destroy(Instantiate(buildFX1, targetPosition + Vector3.up, Quaternion.identity), 3);
        //                }
        //            }
        //        }

        //        break;

        //    #endregion

        //    #region drag

        //    case EditorMode.drag:

        //        if (Input.touchCount == 1)
        //        {
        //            Touch t = Input.touches[0];

        //            switch (t.phase)
        //            {
        //                #region began   //INITIALIZES THE DRAGOBJECT VIA RAYCAST
        //                case TouchPhase.Began:

        //                    Ray ray = Camera.main.ScreenPointToRay(t.position);
        //                    if (Physics.Raycast(ray, out hit))
        //                    {
        //                        if (hit.collider.tag != "FLOOR")
        //                        {
        //                            dragObject = hit.transform;
        //                            dragObject.GetComponent<Collider>().enabled = false;
        //                        }
        //                    }

        //                    break;
        //                #endregion

        //                #region ended   //HANDLES DRAG OBJECT END DRAG LOGIC
        //                case TouchPhase.Ended:

        //                    dragObject.GetComponent<Collider>().enabled = true;
        //                    dragObject.GetComponent<EditorObject>().data.position = transform.position;

        //                    dragObject = default;

        //                    break;
        //                    #endregion

        //                    #region default     //HANDLES DRAG

        //                    if (dragObject != null)
        //                    {
        //                        float x = Mathf.Round(hit.point.x / 4) * 4;

        //                        float height = dragObject.localScale.y;
        //                        float y = Mathf.Round(hit.point.y / (height / 4)) * (height / 4) + height / 4;

        //                        float z = Mathf.Round(hit.point.z / 4) * 4;

        //                        Vector3 targetPosition = new Vector3(x, y, z);

        //                        dragObject.position = targetPosition;
        //                    }



        //                    #endregion
        //            }
        //        }

        //        break;

        //    #endregion

        //    #region delete

        //    case EditorMode.delete:

        //        if (Input.touchCount == 1)
        //        {
        //            Touch myTouch = Input.touches[0];

        //            if (myTouch.phase == TouchPhase.Began)
        //            {
        //                Ray ray = editorCamera.ScreenPointToRay(myTouch.position);
        //                if (Physics.Raycast(ray, out hit))
        //                {
        //                    //tbd : replace with a layermask
        //                    if (hit.collider.tag != "FLOOR")
        //                    {
        //                        Destroy(hit.collider.gameObject);
        //                    }
        //                }
        //            }
        //        }

        //        break;

        //    #endregion

        //    #region playtest

        //    case EditorMode.playtest:

        //        break;

        //    #endregion

        //    default:
        //        break;
        //}
    }
}
