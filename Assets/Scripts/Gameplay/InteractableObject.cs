using UnityEngine;
using UnityEngine.EventSystems;

//base class for interactable objects - handles moving, rotating, deleting?
public class InteractableObject : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RaycastHit hit;

    public void OnPointerDown(PointerEventData eventData)
    {
        EditMode currentMode = GameManager.Instance.editMode;
        if (currentMode == EditMode.delete)
        {
            ObjectDestroyedDelegate?.Invoke();
            Destroy(gameObject);
        }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        StartDrag();
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(eventData.position), out hit))
        {
            float x = Mathf.Round(hit.point.x);
            float height = transform.localScale.y;
            float y = Mathf.Round(hit.point.y / (height / 2)) * (height / 2) + height / 2;
            float z = Mathf.Round(hit.point.z);
            Vector3 targetPosition = new Vector3(x, y, z);
            transform.position = targetPosition;

            //lerp position instead?
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        EndDrag();
    }
    private void StartDrag()
    {
        //handle graphics?
        gameObject.layer = 2;
        //play sound?
    }
    private void EndDrag()
    {
        //handle graphics?
        gameObject.layer = 0;
        //play sound?
    }
    public void Solidify()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (Transform t in transforms)
        {
            t.gameObject.layer = 0;
        }
    }


    //object destruction
    //tbd - using delegates
    public delegate void OnObjectDestroyed();
    public OnObjectDestroyed ObjectDestroyedDelegate;
}
